# BPM
```plantuml
@startbpm
:Firewall;
new branch
:corp monitor;
else
:snmp-2-shell;
end branch
new branch
:Splunk2;
else
:ELastic2;
else
new branch
:ELastic;
else
:Splunk;
end branch
end branch
:DataBoard;
@endbpm
``` 

# For SAP 

In SAP Bank Communication Manager (BCM), a component of SAP Financial Supply Chain Management (FSCM) module, we needed to consider an architecture as follows:
 - Ecc all functional module.
 - PI process integration layer 

```plantuml
ECC ..> PI
PI ..> Bank
ECC --> Bank
```

## From previous 2017, 
 - In SAP Enterprise Portal (EP), the BCM Broadcast Messaging can send and transmit information across the Portal. 
 - For log reading could use BLV tool (SAP CONTACT CENTER/BCM Log Viewer) to check events.




```mermaid
graph TD;
 ECC --> PI  ;
PI --> Bank  ;
ECC --> Bank  ;
```

  -  https://gitlab.com/crlhyn/siterel

 



# Table:

 [[_TOC_]]
