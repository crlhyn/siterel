
### Showing Brief List
    
    grep -E '^[0-9]{3}\s+' --color /tmp/*.txt

    /tmp/1.txt:404 |0.256150| https://armmf.adobe.com/
    /tmp/10.txt:200 |7.233060| https://gem.awmdm.com/GEM/Login/Index?ReturnUrl=%2fGEM%2f
    /tmp/3.txt:404 |0.421143| https://gdata-a.akamaihd.net/
    /tmp/4.txt:404 |0.596890| https://gdmf.apple.com/
    /tmp/5.txt:404 |0.300269| https://mdmenrollment.apple.com/
    /tmp/6.txt:404 |0.554254| https://uclient-api.itunes.apple.com/
    /tmp/8.txt:404 |0.173134| https://analytics.ff.avast.com/
    /tmp/9.txt:204 |0.523205| https://ipm-provider.ff.avast.com/

    
