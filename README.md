# Site-Reliability Reporting tool ? 

This tool checks and reports on health of websites and/or infrastructure, saving time/cost from market tools. This bot helps Site Reliability and Automation, essential parts of agile development; 


SWE can check front/ middle/ backend apps or dbs or verify websites functionality. Systems verify proxies policies .. servers reliability. The free version does not cover more detailed or complex like js checks.

# how to use

 1. Save any [urls](#urls) to check in a text file (one url per line),
 2. Run the bot, it will emulate "click" on each Url listed, and 
 3. See the obtained report: with response time, http code, final URL redirected to... 
 
you can run this either from a shell (git bash) or automatically (add in scheduler or crontab code)

## generic help case

This simple web-check helps in cases like this:
- say	you know 1 url http://frontsite/do/search?q=databse1&q=text  that triggers a DB action you want to watch 
- the team just needs to add those URL to the text files and run the script, then examine the report obtained

When the URLs trigger a DB action in the backend, or certain functionalities to watch, or some infrastructure Routes pending, the report give us the health check of how our system behave against expectations.
 
## next versions
Current script simply clicks on the Urls listed in the input.
Next versions will evolve  :
-	To add cookies able to run with authorized sessions
-	to copy reports to a site visible by more teams : e.g. Jira , KB
-	to group by Expected result 200-299  or 400-499   (e.g. file  UrlList.int.2xx ) and alert on deviations only
- verify if responses contain an expected key string ( such as "/site name/" ).

[remarks]:  # "this not needed in github ; added for gitlab links"
[SiteRel]: #SiteRel "In this document"
[urls]: #urls "Below in this doc" 

## Urls

Good URLs to provide for bot checking: any site urls that trigger key operations or show functionalities. 
For instance: the login URL, any search URL that triggered a dB query, any URL that should trigger a fast redirect, etc. 

 
# Examples of Outputs

 Response | Total Time | From|To | Redirected To URL | ERROR ? | Analysis MEANING | Next step 
---|---|---|---|---|---|---|---
HTTP 302| 0.24| Exterior  |  Mobile GW | to msm |  {- Unexpected 302 -} | missing DNS &/or config | update mobile GW configs
HTTP 200| 0.01| Internal  | Vdemo:8002 | vdemo |   {- expected 403 -} | password-less web | add Reverse/Passwd
301| 0.13| Internal | MCR MS Docker root / | to github !?! | {- Unexpected 301 -} | Policy Well Blocked | policy is succesfull
HTTP 200| 0.16| Internal  | MCR MS Docker proper-URL | https://mcr.microsoft.com/ v2/windows/servercore/ manifests/ltsc2019-amd64 | {+ OK as expected +} | ok| {+ ok -}
IP Lease| 0.9| Internal  | DHCP VLAN M | 1 IP offered - Free | {+ 1IP as expected +} | DHCP ok| {+ ok -}
NO-IP | 0.9| Internal | DHCP VLAN SD / | 0 IP offered | {- Unexpected NO-IP -} |  broken dhcp? | ?

## Report Analysis and Meaning, 
 
2. The detected 302 redirects to an invalid Url. It points out missing DNS &/or config, or need for custom fix.
3. The 200 shows a vulnerability: password-less web  showing sensible data. recurrent issue to opening port 8002/1 
1. The detected 301 points out a MS security risk. Also verified git blocked by the infrastructure proxy policy.
2. the Docker full URL works thought 


## Outcomes, such as mitigations on breaches found 
2. update mobile GW configs
3. Do not let vpn browse port 8002 (due to passwordless web) but open from protective IIS/balancers
1. verifying other MCR endpoints
 
 
# when used externally with Cluster management 
  Checking outside (AWS GCP cloud run) 

Example [cluster management](https://docs.gitlab.com/ee/user/clusters/management_project.html) project.

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/cluster-management).  

 
### dB
theose codes can benefit other teams and projects track websites performance & functionality and the infrastructure behind it.
 
need to request for new roles (browse and publish) in AM to manage the permissions on the report.

### mou

Same understanding shared with other units: the Q2Q3 help, After that cannot give similar help, and the long term running is on each unit.

to replicate:

    for n in $LI  ; do   TO=$n.org;  curl -m8 -LksS -w"%{http_code} | $TO | %{time_total}| %{url_effective} \n" -o /dev/null    $TO ;  done   
or this for sites needing HTTPS

    LI=" sgmsg  sgmuag   sgmdm   msg       app.sg"  #NEED HTTPS
    for n in $LI  ; do   TO=$n.org;  curl -m8 -LksS -w"%{http_code} | $n | %{time_total}| %{url_effective} \n" -o /dev/null    https://$TO ;  done   
 

 
 ```plantuml
user ..> Nginx
Nginx ..> swWeb
```

or 
```mermaid
graph TD;
  int-->B;
  int-->C;
  ext-->D;
  C-->D;
```



# Table:

 [[_TOC_]]

<details>
<summary markdown="span"> click to expand more </summary>
 examples
</details>

---
a: 77
b: 67
...
